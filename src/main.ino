/**
 * (C) Roosembert Palacios, 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * This sketch connects an AirGradient device to a WiFi network and
 * streams datat to an InfluxDB database using the provided credentials.
 *
 * The board is expected to have the following sensors:
 *
 * Plantower PMS5003 (Fine Particle Sensor)
 * SenseAir S8 (CO2 Sensor)
 * SHT30/31 (Temperature/Humidity Sensor)
 *
 * This sketch requires the following libs:
 * - squix78/ESP8266 and ESP32 OLED driver for SSD1306 displays v4.2.0.
 * - airgradienthq/AirGradient v1.3.4.
 */

#include <AirGradient.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <IniFile.h>
#include <LittleFS.h>
#include <Wire.h>

#include "SSD1306Wire.h"

AirGradient ag = AirGradient();
SSD1306Wire display(0x3c, SDA, SCL);

const boolean hasPM = true;
const boolean hasCO2 = true;
const boolean hasSHT = true;
const char* configFilePath = "/cfg.ini";

char wifiSSID[100] = {0};
char wifiPass[100] = {0};
char influxURL[100] = {0};
char influxUser[100] = {0};
char influxPass[100] = {0};

String chipId;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  delay(50);

  if (!LittleFS.begin()) {
    Serial.println("An Error has occurred while mounting LittleFS");
  } else {
    readCfg();
  }

  display.init();
  display.flipScreenVertically();
  chipId = String(ESP.getChipId(), HEX);

  digitalWrite(LED_BUILTIN, LOW);
  showTextRectangle("Init", chipId, true);

  setupWiFi();

  if (hasPM)
    ag.PMS_Init();
  if (hasCO2)
    ag.CO2_Init();
  if (hasSHT)
    ag.TMP_RH_Init(0x44);

  waitForWiFi();
  delay(1000);
}

void readCfg() {
  Serial.println("Reading configuration file.");
  IniFile ini(configFilePath);
  char buffer[300];

  if (!ini.open()) {
    Serial.print("Could not open configuration file ");
    Serial.println(configFilePath);
    return;
  }

  if (!ini.validate(buffer, sizeof(buffer))) {
    Serial.println("Failed to read config file: err " + String(ini.getError()));
    return;
  }

  if (!ini.getValue("wifi", "SSID", wifiSSID, sizeof(wifiSSID)) ||
      !ini.getValue("wifi", "PSK", wifiPass, sizeof(wifiPass)))
    Serial.println("Warning: Could not read WiFi configuration.");

  if (!ini.getValue("Influx", "URL", influxURL, sizeof(influxURL)))
    Serial.println("Warning: Could not read Influx configuration.");

  // Failure to load InfluxDB auth data is acceptable.
  ini.getValue("Influx", "User", influxUser, sizeof(influxUser));
  ini.getValue("Influx", "Pass", influxPass, sizeof(influxPass));
}

struct s_sensors {
  int pm2;
  int co2;
  TMP_RH sht;
};

void loop() {
  struct s_sensors data = {0};
  pollSensors(&data);
  reportSensorData(&data);

  showTextRectangle("PM2", hasPM ? String(data.pm2) : " - ", false);
  delay(3000);

  showTextRectangle("CO2", hasCO2 ? String(data.co2) : " - -", false);
  delay(3000);

  if (hasSHT)
    showTextRectangle(String(data.sht.t), String(data.sht.rh)+"%", false);
  else
    showTextRectangle("No", "SHT", false);
  delay(3000);
}

void pollSensors(struct s_sensors *dst) {
  if (hasPM)
    dst->pm2 = ag.getPM2_Raw();
  if (hasCO2)
    dst->co2 = ag.getCO2_Raw();
  if (hasSHT)
    dst->sht = ag.periodicFetchData();
}

#define INFLUX_LINE(NAME, VALUE) \
  NAME ",device=" + chipId + " value=" + String(VALUE)
#define INFLUX_EXTRA_LINE(NAME, VALUE) \
  "\n" NAME ",device=" + chipId + " value=" + String(VALUE)

void reportSensorData(struct s_sensors *data) {
  if (!influxURL[0]) {
    showTextRectangle("Influx", "Failed", true);
    delay(2000);
    // Not logging to Serial since this function is called from the loop.
    return;
  }

  String payload = INFLUX_LINE("wifi_rssi", WiFi.RSSI());
  if (hasPM)
    payload = payload + INFLUX_EXTRA_LINE("pm02", data->pm2);
  if (hasCO2)
    payload = payload + INFLUX_EXTRA_LINE("rco2", data->co2);
  if (hasSHT) {
    payload = payload + INFLUX_EXTRA_LINE("temp", data->sht.t);
    payload = payload + INFLUX_EXTRA_LINE("rhum", data->sht.rh);
  }

  digitalWrite(LED_BUILTIN, HIGH);

  WiFiClient client;
  HTTPClient http;

  http.begin(client, influxURL);
  if (strlen(influxUser))
    http.setAuthorization(influxUser, influxPass);
  int httpCode = http.POST(payload);

  if (httpCode != 204) {
    Serial.print("HTTP Failed with error code " + String(httpCode) + ": ");
    String msg = String(http.getString());
    msg.trim();
    Serial.println(msg);
    showTextRectangle("HTTP", String(httpCode), false);
    delay(2000);
  }

  http.end();
  digitalWrite(LED_BUILTIN, LOW);
}

void setupWiFi() {
  if (!(wifiSSID[0] && wifiPass[0])) {
    Serial.println("WiFi configuration not available.");
    return;
  }
  Serial.print("Connecting to WiFi Network: '");
  Serial.print(wifiSSID);
  Serial.println("'");
  WiFi.begin(wifiSSID, wifiPass);
}

void waitForWiFi() {
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, HIGH);
    showTextRectangle("WiFi", "init", false);
    delay(300);

    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
  }
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
}

void showTextRectangle(String l1, String l2, boolean small) {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  if (small) {
    display.setFont(ArialMT_Plain_16);
  } else {
    display.setFont(ArialMT_Plain_24);
  }
  display.drawString(32, 16, l1);
  display.drawString(32, 36, l2);
  display.display();
}
