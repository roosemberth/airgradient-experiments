#!/usr/bin/env python3

# This file is a python3 script because PlatformIO only supports python3
# scripts.

import subprocess

script = """
# Preemptively install the required libs to we can patch them
pio lib install

for patchFile in $(find ./patches -type f); do
  if ! patch -R -p0 -s -f --dry-run < "$patchFile" 2> /dev/null; then
    patch -p0 < "$patchFile"
  fi
done

# Unfortunatelly IniFile pulls SD lib before I can patch its dependencies.
rm -fr ".pio/libdeps/d1_mini/SD"
"""

subprocess.run(script, shell=True)
