# PlatformIO project for [AirGradient] Air Quality Sensor

[AirGradient]: https://www.airgradient.com/diy/

The current sketch connects an AirGradient device to a WiFi network and streams
data to an InfluxDB database using the provided credentials.

The wifi and InfluxDB credentials are read from an embedded configuration file.

## Building and Running

Before flashing make sure the `cfg.ini` file exists in the `data/` directory!
You can use the provided configuration example as a start.

All-in-one command for the unpatient:

```
$ platformio run --target uploadfs && sleep 3 && platformio run -j8 --target upload --target monitor
```

You're still with me? Cool, keep reading:

The `data/` directory contains the files to be stored in the root of the
filesystem located in one partition in the system memory.
If you're curious, I'd suggest you to read the documentation on
[partition tables][man-pts].

This project uses LittleFS as partition format and the application configuration
is stored in the file `/cfg.ini`.
This makes easier (and much faster) to change the configuration.
In the future the application could be extended to change it's configuration and
reboot.
It also make possible to redistribute the binary without remorse.

[man-pts]: https://arduino-esp8266.readthedocs.io/en/latest/filesystem.html

### Build and flash the filesystem

```
$ platformio run --target uploadfs
```

### Build the application

```
$ platformio run
```

### Flash the application

```
$ platformio run --target upload
```

### Sample grafana dashboard

![Grafana screenshot displaying collected data](img/grafana-sample.png)

## Patching

Unfortunatelly, the IniFile library seems to lack proper support for LittleFS
on ESP8266, Such support is patched in using a prebuild hook.
See the corresponding section in the `platformio.ini` file.

There's an extra patch for the AirGradient library.
I suspect it was developed on MS Windows or another OS where paths are not case
sensitive.

In both cases I intend to upstream, when I find the time to do so.
Feel free to ninja me if you feel like spending a few hours on it.

License: GPLv3
